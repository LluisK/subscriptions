﻿namespace Subscribe.Contracts
{
    public interface IMonitoring
    {
        void Start();

        void Stop();
    }
}
