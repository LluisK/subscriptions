﻿using System;
using System.IO;
using Subscribe;

namespace TestSubscribe
{
    internal enum ActionType
    {
        forCreate,
        forDelete,
        forModify,
        forRename
    }


    internal class Helper : IDisposable
    {
        private readonly string folder = @"C:\FolderMonitorTemp";
        private const string nameFile = "Point.txt";
        private const string newName = "NewPoint.txt";

        private readonly string newFolderPath;

        private FolderMonitor fm;

        internal bool EventRaised { get; private set; } = default(bool);
        internal object EventSource { get; private set; } = default(object);
        internal FileEventArgs EventArgs { get; private set; } = default(FileEventArgs);
        internal string TempFile { get; }

        internal FolderMonitor MonitorMock
        {
            get
            {
                fm = new FolderMonitor(folder);

                fm.FolderChanged += (obj, args) =>
                {
                    EventRaised = true;
                    EventSource = obj;
                    EventArgs = args;
                };

                fm.EnableRaisingEvents = true;

                return fm;
            }
        }


        internal Helper(ActionType type)
        {
            TempFile = Path.Combine(folder, nameFile);
            newFolderPath = Path.Combine(folder, newName);

            CreateFolder(folder);

            ActionByType(type);
        }

        
        private void ActionByType(ActionType type)
        {
            switch (type)
            {
                case ActionType.forCreate:
                    DeleteFile();
                    break;
                case ActionType.forDelete:
                case ActionType.forModify:
                case ActionType.forRename:
                    DeleteFile();
                    CreateFile();
                    break;
            }
        }

        internal void DeleteFile()
        {
            if (File.Exists(TempFile)) File.Delete(TempFile);
            if (File.Exists(newFolderPath)) File.Delete(newFolderPath);
        }
        
        internal void CreateFile()
        {
            if (!File.Exists(TempFile))
            {
                using (StreamWriter sw = File.CreateText(TempFile))
                    sw.WriteLine("This is a test!!");
            }
        }

        internal void ModifyFile()
        {
            if (File.Exists(TempFile))
            {
                using (StreamWriter sw = File.AppendText(TempFile))
                    sw.WriteLine("This is a test again!");
            }
        }

        internal void RenameFile()
        {
            FileInfo fInfo = new FileInfo(TempFile);
            string destPath = Path.Combine(folder, newName);
            
            fInfo.MoveTo(destPath); 
        }

        internal void CreateFolder(string path) { if (!Directory.Exists(path)) Directory.CreateDirectory(path); }

        internal void DeleteFolder(string path) { if (Directory.Exists(path)) Directory.Delete(path); }


        #region IDisposable 

        private bool disposedValue = false; 

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    EventRaised = false;
                    EventSource = null;
                    EventArgs = null;
                    DeleteFile();
                    DeleteFolder(folder);
                    fm.Dispose();
                }
                
                disposedValue = true;
            }
        }

      
        public void Dispose()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            Dispose(true);
        }
        #endregion


    }
}
