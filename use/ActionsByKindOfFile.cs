﻿using System;
using Subscribe;
using Subscriptions.Subscribers;

namespace Subscriptions
{
    internal static class ActionsByKindOfFile
    {
        internal static ManagingImageFiles.ListenerAction CheckImageFilesInFolder()
        {
            return (object sorce, FileEventArgs arg) =>
            {
                switch (arg.Extension)
                {
                    case ".jpg":
                    case ".gif":
                        Console.WriteLine($"The file { arg.Name } was { arg.ChangeTypes } at { arg.LiteralAccessTime} ");
                        break;
                }
            };
        }
    }
}
