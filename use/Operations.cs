﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using Subscribe;

namespace Subscriptions
{

    public interface IActionsByFile
    {
        void Run(object folder, FileEventArgs fileInfo);
    }



    public class BackupFile : ManagingFiles, IActionsByFile
    {
        private readonly ConcurrentQueue<string> Qfiles;

        public BackupFile()
        {
            Qfiles = new ConcurrentQueue<string>();
        }

        public void Run(object folder, FileEventArgs fileInfo)
        {
            int waiting;
            int.TryParse(ConfigurationManager.AppSettings["wait"].ToString(), out waiting);


            if (fileInfo.Extension.Equals(".txt") && fileInfo.ChangeTypes == WatcherChangeTypes.Created)
            {
                Thread.Sleep(waiting);
                Qfiles.Enqueue(fileInfo.Name);
                ActionByElement();
            }

            ShowActionByFile(fileInfo);
        }
        
        private void ActionByElement()
        {

            if (Qfiles.Any())
            {
                string filename;

                try
                {
                    if (Qfiles.TryDequeue(out filename))
                    {
                        string file = Path.Combine(ConfigurationManager.AppSettings["OriginFile"], filename);
                        int nLines;
                        int dataCalcul = CalculaValueFile(file, out nLines);

                        WriteInfoToAFile($"File: { filename} [Lines {nLines.ToString()}] Sum ()=> {dataCalcul.ToString()} // Date { DateTime.Now } ");
                    }
                }
                catch (Exception e)
                {
                    WriteInfoToAFile($"Error to Dequeue: { e.Message } / {e.InnerException} Trace: {e.StackTrace} ");
                }
            }
        }
        
        private void WriteInfoToAFile(string info)
        {
            string path = Path.Combine(ConfigurationManager.AppSettings["DestFile"], "Results.txt");

            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(info + Environment.NewLine);
            }
        }

        private int CalculaValueFile(string nameFile, out int nLines)
        {
            int result = 0;
            string line;
            nLines = 0;
            using (StreamReader reader = new StreamReader(nameFile))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    nLines++;
                    Array.ForEach(line.Split(';'), (string i) => { result += Convert.ToInt32(i); });
                }
            }

            return result;
        }

        protected void ShowActionByFile(FileEventArgs fileInfo)
        {
            if (fileInfo.Extension.Equals(".txt"))
            {
                change = fileInfo.ChangeTypes;

                string oldName;

                oldName = (change == WatcherChangeTypes.Renamed) ? $"Old name { fileInfo.OldName } - " : string.Empty;

                Console.WriteLine($"{oldName}{KindOfChange }: { fileInfo.Name } -- { fileInfo.LiteralAccessTime} ");
            }

        }
    }
}
