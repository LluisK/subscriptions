﻿using System;
using System.Collections.Generic;
using Subscribe;

namespace Subscriptions
{
    class Program
    {
        protected Program() { }

        static void Main()
        {
            List<Monitoring> lstMonitoring = new List<Monitoring>()
            {
                BuildMonitor.FilesTxtInFolder(),
                BuildMonitor.FilesImageThroughDelegate()
            };


            lstMonitoring
             .ForEach((Monitoring item) =>
            { item.Start(); });


            Console.WriteLine("Press any key to stop the listeners ...");
            Console.ReadKey();

            lstMonitoring
            .ForEach((Monitoring item) =>
            { item.Stop(); });


        }
    } 
}
